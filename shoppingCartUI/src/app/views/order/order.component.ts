import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { GlobalConfig } from 'src/app/app.globals';
import { CodeRef } from 'src/app/model/code-ref.model';
import { CommitStatus } from 'src/app/model/commit-status.model';
import { Order } from 'src/app/model/order.model';
import { Product } from 'src/app/model/product.model';
import { OrderService } from 'src/app/service/order.service';
import { ProductService } from 'src/app/service/product.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})
export class OrderComponent implements OnInit {

  public tableViewList: Order[]=[];
  public selectedRecords: Order[]=[];
  public cols: any[];
  public totalRecords: number;
  public startRow: number;
  public endRow: number;
  public rowIdCounter: number = 0;
  public currentUser: any;
  public inputDisabled: boolean = false;
  public gridTableClassName: string;
  public buttonDisabled: boolean = true;
  public blocked: boolean;
  public customerId : number;
  public productList : CodeRef[]=[];
  public productListRef : Product[]=[];
  public displayDialog: boolean;
  public order : Order = new Order();
  public commitStatus : CommitStatus;
  public grandTotal : number;

  constructor(
    private activeRoute: ActivatedRoute,
    public config: GlobalConfig,
    private router: Router,
    private messageService: MessageService,
    private orderService : OrderService,
    private productService : ProductService
  ) { }

  async ngOnInit() {
    this.cols = [
      { field: 'orderId', header: 'Order Id', width: '100px', textAlign: 'left'},
      { field: 'productId', header: 'Product', width: '150px', textAlign: 'left'},
      { field: 'price', header: 'Price', width: '150px', textAlign: 'left'},
      { field: 'quantity', header: 'Quantity', width: '100px', textAlign: 'left'},
      { field: 'totalPrice', header: 'Total Price', width: '100px', textAlign: 'right'},
    ];
    this.totalRecords = 0;
    this.grandTotal=0;
    this.gridTableClassName = 'col-md-12 non-editable';
    this.getProductList();
    this.customerId = Number(this.activeRoute.snapshot.paramMap.get('customerId'));
    console.log(this.customerId);
    this.loadData(true)
  }

  getProductList() {
    this.productService.getProductList()
    .subscribe(result => {
        console.log("result list size : " + result.length);
        this.productListRef=result;
        this.productList = new Array<CodeRef>();
        for (let rowData of result) {
           let codeRef : CodeRef = new CodeRef();
           codeRef.label = rowData.productDesc;
           codeRef.value = rowData.productId;
           this.productList.push (codeRef);
        }
      },
      error => {
       console.log("Error = " + error.message);
      }
    );
  }

  loadData(displayMsg : boolean) {
    this.blocked = true;
    this.orderService.loadData(this.customerId)
    .subscribe(result => {
        
      console.log("result.tableViewList size : " + result.length);
      this.tableViewList =result;
      this.totalRecords=this.tableViewList.length;
      this.buttonDisabled = true;
      this.inputDisabled = true;
      this.gridTableClassName = 'col-md-12';
      this.blocked = false;

      this.grandTotal=0;
      for (let rowData of result) {
        this.grandTotal=this.grandTotal + rowData.totalPrice;
      }

      /*if (displayMsg) {
        this.messageService.add({severity: 'info', summary: 'Info', life: 2000,
          detail: 'Returned ' + this.tableViewList.length + ' row(s) of data'});
      }*/
    },
      error => {
      console.log("Error = " + error.message);
      this.blocked = false;
      }
    );
  }

  onClickAddItem() {
    this.displayDialog = true;
    this.order = new Order();
    this.order.customerId=this.customerId;
    this.buttonDisabled=true;
  }

  getProductPrice(){
    for (let rowData of this.productListRef ) {
      if(rowData.productId==this.order.productId) {
        this.order.price=rowData.price;
      }
    }
  }

  getTotalPrice() {
    this.buttonDisabled=false;
    this.order.totalPrice=this.order.price * this.order.quantity;
  }

  onClickRemoveItem() {
    this.removeCurstomerOrderData();
  }

  removeCurstomerOrderData() {
    this.blocked = true;
    this.orderService.removeCurstomerOrderData(this.selectedRecords)
    .subscribe(result => {
      this.commitStatus = result;
      this.buttonDisabled = false;
      this.inputDisabled = true;
      this.blocked = false;

      if (this.commitStatus.status=="success") {
        for (let rowData of this.selectedRecords ) {
          const idx: number = this.tableViewList.indexOf(rowData);
          if (idx !== -1) {
            this.tableViewList.splice(idx, 1);
          }   
        }
        //clear selected records
        this.selectedRecords = new Array<Order>();
        this.grandTotal=0;
        for (let rowData of this.tableViewList) {
          this.grandTotal=this.grandTotal + rowData.totalPrice;
        }
        this.totalRecords=this.tableViewList.length;
        this.messageService.add({severity: 'info', summary: 'Info', life: 2000,
          detail: 'Item(s) were successfully removed !'});
      }
      if(this.commitStatus.status=="failed") {
        this.messageService.add({severity: 'error', summary: 'Error',
        detail: 'Error occurred during saving of data!'});
      }
    },
      error => {
      console.log("Error = " + error.message);
      this.blocked = false;
      }
    );
  }

  onClickSelectRow() {
    if (this.selectedRecords.length>0) {
      this.buttonDisabled=false;
    } else {
      this.buttonDisabled=true;
    } 
  }

  onClickBack() {
    this.router.navigate(['/customer']);
  }

  onClickSaveItem() {
    this.displayDialog = false;
    this.saveCurstomerOrderData();
  }
  
  saveCurstomerOrderData() {
    this.blocked = true;
    this.orderService.saveCurstomerOrderData(this.order)
    .subscribe(result => {
      this.commitStatus = result;
      this.buttonDisabled = false;
      this.inputDisabled = true;
      this.blocked = false;

      if (this.commitStatus.status=="success") {
        this.loadData(false);
        this.messageService.add({severity: 'info', summary: 'Info', life: 2000,
          detail: 'Item were successfully saved!'});
      }
      if(this.commitStatus.status=="failed") {
        this.messageService.add({severity: 'error', summary: 'Error',
        detail: 'Error occurred during saving of data!'});
      }
    },
      error => {
      console.log("Error = " + error.message);
      this.blocked = false;
      }
    );
  }

  onClickCancel() {
    this.displayDialog = false;
  }

  pagination(event) {
    //event.first: Index of first rowData being displayed 
    //event.rows: Number of rows to display in new page 
    //event.page: Index of the new page 
    //event.pageCount: Total number of pages
    this.startRow = event.first;
    this.endRow =  this.startRow + event.rows;
  }

}
