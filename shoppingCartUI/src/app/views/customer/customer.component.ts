import { Component, OnInit, ViewChild } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ConfirmationService, MessageService } from 'primeng/api';
import { Table } from 'primeng/table';
import { GlobalConfig } from 'src/app/app.globals';
import { CommitStatus } from 'src/app/model/commit-status.model';
import { Customer } from 'src/app/model/customer.model';
import { CustomerService } from 'src/app/service/customer.service';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss']
})
export class CustomerComponent implements OnInit {

  public tableViewList: Customer[]=[];
  public selectedRecords: Customer[]=[];
  public cols: any[];
  public totalRecords: number;
  public startRow: number;
  public endRow: number;
  public rowIdCounter: number = 0;
  public currentUser: any;
  public inputDisabled: boolean = false;
  public gridTableClassName: string;
  public buttonDisabled: boolean = true;
  public blocked: boolean;
  public displayDialog: boolean;
  public customer : Customer =  new Customer();
  public commitStatus : CommitStatus;

  @ViewChild(Table, {static: false}) 
  tableComponent: Table;

  constructor(
    private router: Router,
    private messageService: MessageService,
    private customerService: CustomerService,
    private confirmationService: ConfirmationService,
    public config: GlobalConfig
  ) { }
  
  async ngOnInit() {

    this.cols = [
      { field: 'customerId', header: 'Customer Id', width: '80px', textAlign: 'left'},
      { field: 'fullName', header: 'Fullname', width: '200px', textAlign: 'left'},
      { field: 'birthDate', header: 'Birth Date', width: '100px', textAlign: 'left'},
      { field: 'contactNr', header: 'Contact Number', width: '100px', textAlign: 'left'},
      { field: 'address', header: 'Address', width: '300px', textAlign: 'left'}
    ];

    this.totalRecords = 0;
    this.gridTableClassName = 'col-md-12 non-editable';
    this.loadData(true);
  }

  loadData(displayMsg : boolean) {
    this.blocked = true;
    this.customerService.loadData()
    .subscribe(result => {
        
      console.log("result.tableViewList size : " + result.length);
      this.totalRecords=result.length;
      this.tableViewList =result;
      this.totalRecords=this.tableViewList.length;
      this.buttonDisabled = false;
      this.inputDisabled = true;
      this.gridTableClassName = 'col-md-12';
      this.blocked = false;

      /*if (displayMsg) {
        this.messageService.add({severity: 'info', summary: 'Info', life: 2000,
          detail: 'Returned ' + this.tableViewList.length + ' row(s) of data'});
      }*/
    },
      error => {
      console.log("Error = " + error.message);
      this.blocked = false;
      }
    );
  }

  onClickAddCustomer() {
    this.displayDialog = true;
    this.customer = new Customer();
    this.buttonDisabled=true;
  }

  onClickSaveCustomer() {
    this.displayDialog = false;
    this.saveCustomerData()
    this.buttonDisabled=false;
  }

  saveEnabled() {
    if (this.customer.fullName!=null && this.customer.birthDate!=null 
        && this.customer.contactNr!==null && this.customer.address!=null)  
    this.buttonDisabled=false;
  }

  saveCustomerData() {
    this.blocked = true;
    this.customerService.saveCustomerData(this.customer)
    .subscribe(result => {
      this.commitStatus = result;
      this.buttonDisabled = false;
      this.inputDisabled = true;
      this.blocked = false;

      if (this.commitStatus.status=="success") {
        this.loadData(false);
        this.messageService.add({severity: 'info', summary: 'Info', life: 2000,
          detail: 'Customer Records were successfully saved!'});
      }
      if(this.commitStatus.status=="failed") {
        this.messageService.add({severity: 'error', summary: 'Error',
        detail: 'Error occurred during saving of data!'});
      }
    },
      error => {
      console.log("Error = " + error.message);
      this.blocked = false;
      }
    );
  }
  
  onClickCancel() {
    this.displayDialog = false;
  }

  onClickViewBasket(customerId:number) {
    this.router.navigate(['/order', { customerId : customerId}]);
  }

  pagination(event) {
    //event.first: Index of first rowData being displayed 
    //event.rows: Number of rows to display in new page 
    //event.page: Index of the new page 
    //event.pageCount: Total number of pages
    this.startRow = event.first;
    this.endRow =  this.startRow + event.rows;
  }
}

