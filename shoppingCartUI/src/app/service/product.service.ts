import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AppConfig, APP_CONFIG } from '../app.config';
import { Product } from '../model/product.model';

@Injectable()
export class ProductService {

    constructor(
        private httpClient: HttpClient,
        @Inject(APP_CONFIG) private config: AppConfig
    ) {}
    
    public getProductList() {
        return this.httpClient
        .get<Array<Product>>(this.config.apiEndpoint +'/product/getProductList')
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }

}