import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AppConfig, APP_CONFIG } from '../app.config';
import { CommitStatus } from '../model/commit-status.model';
import { Order } from '../model/order.model';

@Injectable()
export class OrderService {

    constructor(
        private httpClient: HttpClient,
        @Inject(APP_CONFIG) private config: AppConfig
    ) {}
    
    public loadData(customerId:any) {
        return this.httpClient
        .get<Array<Order>>(this.config.apiEndpoint +'/order/getCurstomerOrderList?customerId='+customerId)
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }

    public saveCurstomerOrderData(order: Order) {
        return this.httpClient
        .post<CommitStatus>(this.config.apiEndpoint +'/order/saveCurstomerOrderData', 
            order)
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }

    public removeCurstomerOrderData(selectedRecords: Order[]) {
        return this.httpClient
        .post<CommitStatus>(this.config.apiEndpoint +'/order/removeCurstomerOrderData', 
            selectedRecords)
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }


}