import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { AppConfig, APP_CONFIG } from '../app.config';
import { CodeRef } from '../model/code-ref.model';

@Injectable()
export class CodeRefService {

    constructor(
        private httpClient: HttpClient,
        @Inject(APP_CONFIG) private config: AppConfig
    ) {}
    
    public getRegistrationTypeByIdType(idTypGrpNm : string) {
        return this.httpClient
        .get<Array<CodeRef>>(this.config.apiEndpoint +'/codeRef/getRegistrationTypeByIdType?idTypGrpNm='+idTypGrpNm)
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }

    public getCompanyType() {
        return this.httpClient
        .get<Array<CodeRef>>(this.config.apiEndpoint +'/codeRef/getCompanyType')
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }

    public getBusinessNatureTypeByCmpnyTypCd(cmpnyTypCd : number) {
        return this.httpClient
        .get<Array<CodeRef>>(this.config.apiEndpoint +'/codeRef/getBusinessNatureTypeByCmpnyTypCd?cmpnyTypCd='+cmpnyTypCd)
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }

    public getLanguageType() {
        return this.httpClient
        .get<Array<CodeRef>>(this.config.apiEndpoint +'/codeRef/getLanguageType')
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }

    public getCountryType() {
        return this.httpClient
        .get<Array<CodeRef>>(this.config.apiEndpoint +'/codeRef/getCountryType')
        .pipe(map(result => {
            return result; 
        },
          (err: { Message: any; }) => {
            console.log(err.Message);
        }));
    }

}