export class Order {
    constructor(
        public orderId?: number,
        public customerId?: number,
        public productId?: number,
        public price?: number,
        public quantity?: number,
        public totalPrice?: number){
    }
}