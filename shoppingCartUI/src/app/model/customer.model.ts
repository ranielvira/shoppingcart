export class Customer {
    constructor(
        public customerId?: number,
        public fullName?: string,
        public birthDate?: Date,
        public contactNr?: string,
        public address?: string){
    }
}