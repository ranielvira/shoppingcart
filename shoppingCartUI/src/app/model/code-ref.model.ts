export class CodeRef {
    constructor(
        public label?: string,
        public value?: number){
    }
}