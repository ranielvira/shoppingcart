import { Routes } from '@angular/router';
import { CustomerComponent } from './views/customer/customer.component';
import { NoContentComponent } from './views/no-content/no-content.component';
import { OrderComponent } from './views/order/order.component';

export const ROUTES: Routes = [
  { path: '', redirectTo: 'customer', pathMatch: 'full'},
  { path: 'customer', component: CustomerComponent },
  { path: 'home', component: CustomerComponent },
  { path: 'order', component: OrderComponent },
  { path: '**', component: NoContentComponent }
];
