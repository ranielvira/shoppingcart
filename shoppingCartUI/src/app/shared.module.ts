import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { MessageService, ConfirmationService } from 'primeng/api';
import { TabViewModule } from 'primeng/tabview';
import { RadioButtonModule } from 'primeng/radiobutton';
import { CheckboxModule } from 'primeng/checkbox';
import { ButtonModule } from 'primeng/button';
import { GalleriaModule } from 'primeng/galleria';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { InputTextModule } from 'primeng/inputtext';
import { CalendarModule } from 'primeng/calendar';
import { TooltipModule } from 'primeng/tooltip';
import { ToastModule } from 'primeng/toast';
import { DialogService, DynamicDialogModule, DynamicDialogRef } from 'primeng/dynamicdialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { BlockUIModule } from 'primeng/blockui';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DialogModule } from 'primeng/dialog';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { FileUploadModule } from 'primeng/fileupload';
import { PickListModule } from 'primeng/picklist';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { FieldsetModule } from 'primeng/fieldset';
import { KeyFilterModule } from 'primeng/keyfilter';
import { MessagesModule } from 'primeng/messages';
import { MultiSelectModule } from 'primeng/multiselect';
import { SpinnerModule } from 'primeng/spinner';
import { EditorModule } from 'primeng/editor';
import { CardModule } from 'primeng/card';

import {TabMenuModule} from 'primeng/tabmenu';
import { GlobalConfig } from './app.globals';
import { DropdownModule } from 'primeng/dropdown';
import {PanelModule} from 'primeng/panel';



@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    TabMenuModule,
    TabViewModule,
    RadioButtonModule,
    CheckboxModule,
    ButtonModule,
    GalleriaModule,
    TableModule,
    PaginatorModule,
    InputTextModule,
    CalendarModule,
    TooltipModule,
    ToastModule,
    DynamicDialogModule,
    ProgressSpinnerModule,
    BlockUIModule,
    OverlayPanelModule,
    DialogModule,
    FullCalendarModule,
    FileUploadModule,
    PickListModule,
    InputTextareaModule,
    FieldsetModule,
    KeyFilterModule,
    MessagesModule,
    EditorModule,
    SpinnerModule,
    MultiSelectModule,
    DropdownModule,
    CardModule,
    PanelModule
  ],
  providers: [
    MessageService,
    ConfirmationService,
    GlobalConfig,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ConfirmDialogModule,
    TabViewModule,
    RadioButtonModule,
    CheckboxModule,
    ButtonModule,
    GalleriaModule,
    TableModule,
    PaginatorModule,
    InputTextModule,
    CalendarModule,
    TooltipModule,
    ToastModule,
    DynamicDialogModule,
    ProgressSpinnerModule,
    BlockUIModule,
    OverlayPanelModule,
    DialogModule,
    FullCalendarModule,
    FileUploadModule,
    PickListModule,
    InputTextareaModule,
    FieldsetModule,
    KeyFilterModule,
    MessagesModule,
    TabMenuModule,
    SpinnerModule,
    MultiSelectModule,
    EditorModule,
    DropdownModule,
    CardModule,
    PanelModule
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
      providers: [
        MessageService,
        DialogService,
        DynamicDialogRef,
        ConfirmationService
      ],
    };
  }
}

