package com.shoppingcart.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import com.shoppingcart.dto.ProductDto;
import com.shoppingcart.model.Product;

@Repository
public class ProductDaoImpl extends AbstractDao<Product>{
	
	protected static final Logger logger = LogManager.getLogger(ProductDaoImpl.class);
	
	/**
	 * Find All Product record.
	 */
	public List<ProductDto> getProductList() {
		logger.debug("find all product instance");
		try {
			String queryFormat = "SELECT * FROM product ORDER BY product_desc";

			String sql = queryFormat;
			NativeQuery query = (NativeQuery) getSession().createNativeQuery(sql);

			List<Object[]> results = query.list();
			
			List<ProductDto> resultList = new ArrayList<ProductDto>();
			
			if (results.size() > 0) {
				for (Object[] result : results) {
					ProductDto dto = new ProductDto();
					
					dto.setProductId((int) result[0]);
					dto.setProductDesc((String) result[1]);
					dto.setPrice((Double) result[2]);
					resultList.add(dto);
				}
			}
			logger.debug("find all product successful, result size: " + results.size());
			return resultList;
		} catch (RuntimeException re) {
			logger.error(re.getMessage(), re);
			logger.error("find product failed", re);
			throw re;
		}
	}
	
}