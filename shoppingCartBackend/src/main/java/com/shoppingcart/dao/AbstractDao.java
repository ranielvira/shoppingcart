package com.shoppingcart.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

public class AbstractDao<T> {

	protected static final Logger logger = LogManager.getLogger(AbstractDao.class);

	@Autowired
	protected SessionFactory sessionFactory;
	
	protected final Class<T> persistentClass;

	@PersistenceContext(unitName = "userEntityManager")
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	public AbstractDao() {
		this.persistentClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public Session getSession() {
		return getSession(true);
	}
	
	/**
	 * When using this method in DAO classes: <p>
	 * DO NOT wrap hibernate calls in session.doWork() <p>
	 * DO use hibernate session.createQuery() or session.createStoredProcedureCall() <p>
	 * DO throw back exception from catch clause (if applicable)
	 * DO add @Transactional annotation to service class method using DAO method
	 * @return hibernate Session object
	 */
	public Session getNewSession() {
		return Optional.of(entityManager.unwrap(Session.class)).get();
	}

	protected Session getSession(boolean requiredFilter) {
		Session result = null;
		result = sessionFactory.getCurrentSession();
		return result;
	}

	public List<T> findAll() {
		logger.debug("finding " + persistentClass.getSimpleName() + " instance by example");
		try {
			List<T> results = getSession().createQuery("FROM " + persistentClass.getSimpleName(), persistentClass)
					.list();
			logger.debug("find by example successful, result size: " + results.size());
			return results;
		} catch (RuntimeException re) {
			logger.error("find by example failed", re);
			throw re;
		}
	}
	
	public void persist(T transientInstance) {
		logger.debug("persisting " + persistentClass.getSimpleName() + "  instance");
		try {
			Transaction tx = getSession().beginTransaction();
			getSession().persist(transientInstance);
			tx.commit();
			logger.debug("persist successful");
		} catch (RuntimeException re) {
			logger.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(T instance) {
		logger.debug("attaching dirty " + persistentClass.getSimpleName() + "  instance");
		try {
			Transaction tx = getSession().beginTransaction();
			getSession().saveOrUpdate(instance);
			tx.commit();
			logger.debug("attach successful");
		} catch (RuntimeException re) {
			logger.error("attach failed", re);
			throw re;
		}
	}

	public void save(T instance) {
		logger.debug("saving dirty " + persistentClass.getSimpleName() + "  instance");
		try {
			Transaction tx = getSession().beginTransaction();
			getSession().save(instance);
			tx.commit();
			logger.debug("attach successful");
		} catch (RuntimeException re) {
			logger.error("attach failed", re);
			throw re;
		}
	}
	
	public void saveWithNoCommit(T instance) {
		logger.debug("saving dirty " + persistentClass.getSimpleName() + "  instance");
		try {
//			Transaction tx = getSession().beginTransaction();
			getSession().save(instance);
//			tx.commit();
			logger.debug("attach successful");
		} catch (RuntimeException re) {
			logger.error("attach failed", re);
			throw re;
		}
	}
	
	public void attachClean(T instance) {
		logger.debug("attaching clean " + persistentClass.getSimpleName() + "  instance");
		try {
			getSession().lock(instance, LockMode.NONE);
			logger.debug("attach successful");
		} catch (RuntimeException re) {
			logger.error("attach failed", re);
			throw re;
		}
	}

	public void detach(T persistentInstance) {
		logger.debug("detaching " + persistentClass.getSimpleName() + "  instance");
		try {
			getSession().detach(persistentInstance);
			logger.debug("detach successful");
		} catch (RuntimeException re) {
			logger.error("detach failed", re);
			throw re;
		}
	}

	public void delete(T persistentInstance) {
		logger.debug("deleting " + persistentClass.getSimpleName() + "  instance");
		try {
			Transaction tx = getSession().beginTransaction();
			getSession().delete(persistentInstance);
			tx.commit();
			logger.debug("delete successful");
		} catch (RuntimeException re) {
			logger.error("delete failed", re);
			throw re;
		}
	}
	
	public void deleteWithNoCommit(T persistentInstance) {
		logger.debug("deleting " + persistentClass.getSimpleName() + "  instance");
		try {
			getSession().delete(persistentInstance);
			logger.debug("delete successful");
		} catch (RuntimeException re) {
			logger.error("delete failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public T merge(T detachedInstance) {
		logger.debug("merging " + persistentClass.getSimpleName() + "  instance");
		try {
			Transaction tx = getSession().beginTransaction();
			T result = (T) getSession().merge(detachedInstance);
			tx.commit();
			logger.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			logger.error("merge failed", re);
			throw re;
		}
	}
	
	public void update(T instance) {
		logger.debug("attaching dirty " + persistentClass.getSimpleName() + "  instance");
		try {
			Transaction tx = getSession().beginTransaction();
			getSession().update(instance);
			tx.commit();
			logger.debug("attach successful");
		} catch (RuntimeException re) {
			logger.error("attach failed", re);
			throw re;
		}
	}
	
	public void updateWithNoCommit(T instance) {
		logger.debug("attaching dirty " + persistentClass.getSimpleName() + "  instance");
		try {
//			Transaction tx = getSession().beginTransaction();
			getSession().update(instance);
//			tx.commit();
			logger.debug("attach successful");
		} catch (RuntimeException re) {
			logger.error("attach failed", re);
			throw re;
		}
	}
	
	public void saveOrUpdate(T instance) {
		logger.debug("save or update dirty " + persistentClass.getSimpleName() + "  instance");
		try {
			//Transaction tx = getSession().beginTransaction();
			getSession().saveOrUpdate(instance);
			//tx.commit();
			logger.debug("save or update successful");
		} catch (RuntimeException re) {
			logger.error("save or Update failed", re);
			throw re;
		}
	}
	
	/**
	 * Flushing the session forces Hibernate to synchronize the in-memory state of the Session with the database
	 */
	public void flush() {
		logger.debug("flush session");
		try {
			Transaction tx = getSession().beginTransaction();
			getSession().flush();
			tx.commit();
			logger.debug("flush successful");
		} catch (RuntimeException re) {
			logger.error("flush failed", re);
			throw re;
		}
	}

}
