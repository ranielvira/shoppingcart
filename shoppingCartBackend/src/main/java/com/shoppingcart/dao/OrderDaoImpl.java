package com.shoppingcart.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import com.shoppingcart.dto.OrderDto;
import com.shoppingcart.model.Order;

@Repository
public class OrderDaoImpl extends AbstractDao<Order>{
	
	protected static final Logger logger = LogManager.getLogger(OrderDaoImpl.class);
	
	/**
	 * Find All Order record.
	 */
	public List<OrderDto> getCurstomerOrderList(int customerId) {
		logger.debug("find all order instance");
		try {
			String queryFormat = "SELECT * FROM shoppingcartdb.order WHERE customer_id=:1 ORDER BY product_id";

			String sql = queryFormat;
			NativeQuery query = (NativeQuery) getSession().createNativeQuery(sql);
			query.setParameter("1", customerId);
			List<Object[]> results = query.list();
			
			List<OrderDto> resultList = new ArrayList<OrderDto>();
			
			if (results.size() > 0) {
				for (Object[] result : results) {
					OrderDto dto = new OrderDto();
					
					dto.setOrderId((int) result[0]);
					dto.setCustomerId((int) result[1]);
					dto.setProductId((int) result[2]);
					dto.setPrice((double) result[3]);
					dto.setQuantity((int) result[4]);
					dto.setTotalPrice((double) result[5]);
					resultList.add(dto);
				}
			}
			logger.debug("find all order successful, result size: " + results.size());
			return resultList;
		} catch (RuntimeException re) {
			logger.error(re.getMessage(), re);
			logger.error("find order failed", re);
			throw re;
		}
	}
	
	/**
	 * Create Order/Item record.
	 * @param OrderDto
	 */
	public void saveCurstomerOrderData(OrderDto order) {
		logger.debug("Create order record instance");
		try {
			String queryFormat = "INSERT INTO shoppingcartdb.order VALUES(:1, :2, :3, :4, :5, :6)";

			String sql = queryFormat;
			
			Transaction tx = getSession().beginTransaction();
			NativeQuery query = (NativeQuery) getSession().createNativeQuery(sql);
			query.setParameter("1", null);
			query.setParameter("2", order.getCustomerId());
			query.setParameter("3", order.getProductId());
			query.setParameter("4", order.getPrice());
			query.setParameter("5", order.getQuantity());
			query.setParameter("6", order.getTotalPrice());
			query.executeUpdate();
			tx.commit();
			logger.debug("Create New Order record successful");
		} catch (RuntimeException re) {
			logger.error(re.getMessage(), re);
			logger.error("Create New Order record failed", re);
			throw re;
		}
	}
	
	/**
	 * Remove Item record.
	 * @param orderId
	 */
	public void removeCurstomerOrderData(int orderId) {
		logger.debug("Delete order record instance");
		try {
			String queryFormat = "DELETE FROM shoppingcartdb.order WHERE order_id=:1";

			String sql = queryFormat;
			
			Transaction tx = getSession().beginTransaction();
			NativeQuery query = (NativeQuery) getSession().createNativeQuery(sql);
			query.setParameter("1", orderId);
			query.executeUpdate();
			tx.commit();
			logger.debug("Delete Order record successful");
		} catch (RuntimeException re) {
			logger.error(re.getMessage(), re);
			logger.error("Delete Order record failed", re);
			throw re;
		}
	}
}