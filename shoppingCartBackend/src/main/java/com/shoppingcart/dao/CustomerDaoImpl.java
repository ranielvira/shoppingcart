package com.shoppingcart.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Transaction;
import org.hibernate.query.NativeQuery;
import org.springframework.stereotype.Repository;

import com.shoppingcart.dto.CustomerDto;
import com.shoppingcart.model.Customer;

@Repository
public class CustomerDaoImpl extends AbstractDao<Customer>{
	
	protected static final Logger logger = LogManager.getLogger(CustomerDaoImpl.class);
	
	/**
	 * Find All Customer record.
	 */
	public List<CustomerDto> getCustomerList() {
		logger.debug("find all customer instance");
		try {
			String queryFormat = "SELECT * FROM customer ORDER BY full_name";

			String sql = queryFormat;
			NativeQuery query = (NativeQuery) getSession().createNativeQuery(sql);

			List<Object[]> results = query.list();
			
			List<CustomerDto> resultList = new ArrayList<CustomerDto>();
			
			if (results.size() > 0) {
				for (Object[] result : results) {
					CustomerDto dto = new CustomerDto();
					
					dto.setCustomerId((int) result[0]);
					dto.setFullName((String) result[1]);
					dto.setBirthDate((Date) result[2]);
					dto.setContactNr((String) result[3]);
					dto.setAddress((String) result[4]);
					resultList.add(dto);
				}
			}
			logger.debug("find all customer successful, result size: " + results.size());
			return resultList;
		} catch (RuntimeException re) {
			logger.error(re.getMessage(), re);
			logger.error("find customer failed", re);
			throw re;
		}
	}
	
	/**
	 * Create Customer record.
	 * @param CustomerDto
	 */
	public void saveCustomerData(CustomerDto customer) {
		logger.debug("Create customer record instance");
		try {
			String queryFormat = "INSERT INTO customer VALUES(:1, :2, :3, :4, :5)";

			String sql = queryFormat;
			
			Transaction tx = getSession().beginTransaction();
			NativeQuery query = (NativeQuery) getSession().createNativeQuery(sql);
			query.setParameter("1", null);
			query.setParameter("2", customer.getFullName());
			query.setParameter("3", customer.getBirthDate());
			query.setParameter("4", customer.getContactNr());
			query.setParameter("5", customer.getAddress());
			query.executeUpdate();
			tx.commit();
			logger.debug("Create New Customer record successful");
		} catch (RuntimeException re) {
			logger.error(re.getMessage(), re);
			logger.error("Create New Customer record failed", re);
			throw re;
		}
	}
}