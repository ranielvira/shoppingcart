package com.shoppingcart.dto;

import java.util.Date;

public class CustomerDto implements java.io.Serializable {

	private int customerId;
	private String fullName;
	private Date birthDate;
	private String contactNr;
	private String address;

	public CustomerDto() {
	}

	public CustomerDto(int customerId) {
		this.customerId = customerId;
	}

	public CustomerDto(int customerId, String fullName, Date birthDate, String contactNr, String address) {
		this.customerId = customerId;
		this.fullName = fullName;
		this.birthDate = birthDate;
		this.contactNr = contactNr;
		this.address = address;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getContactNr() {
		return contactNr;
	}

	public void setContactNr(String contactNr) {
		this.contactNr = contactNr;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

}
