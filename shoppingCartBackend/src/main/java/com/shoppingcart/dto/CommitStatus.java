package com.shoppingcart.dto;

public class CommitStatus {
	
	private String status;
	private String errMsg;
	
	public CommitStatus() {
	}

	public CommitStatus(String status, String errMsg) {
		super();
		this.status = status;
		this.errMsg = errMsg;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

}
