package com.shoppingcart.configs;
import java.beans.PropertyVetoException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableAsync
@EnableTransactionManagement
@EnableJpaRepositories(
    basePackages = { "com.shoppingcart" }, 
    entityManagerFactoryRef = "userEntityManager", 
    transactionManagerRef = "userTransactionManager"
)

@PropertySource(value="classpath:application.properties")
public class PersistenceConfiguration implements WebMvcConfigurer{
	
	protected static final Logger logger = LogManager.getLogger(PersistenceConfiguration.class);

    @Value("${spring.datasource.jdbc-url}")
    private String URL;
    
    @Value("${spring.datasource.username}")
    private String USERNAME;
    
    @Value("${spring.datasource.password}")
    private String PASSWORD;
    
    @Value("${hibernate.dialect}")
    private String DIALECT;
    
    @Value("${hibernate.hbm2ddl.auto}")
    private String HBM2DLL;
    
    @Value("${hibernate.show_sql}")
    private String HSHOW_SQL;
    
    @Value("${hibernate.proc.param_null_passing}")
    private String PROC_NULL_PASSING;
    
    @Bean(name = "dataSource")
    public DataSource dataSource() throws NamingException, SQLException{
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setUrl(URL);
        dataSource.setUsername(USERNAME);
        dataSource.setPassword(PASSWORD);
        return dataSource;
    }
	
	@Bean
	@Primary
    public LocalSessionFactoryBean sessionFactory() throws NamingException, PropertyVetoException, SQLException{
        LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
        sessionFactory.setDataSource(dataSource());
        sessionFactory.setPackagesToScan(new String[] {"com.shoppingcart.model"});
        Properties hibernateProperties = new Properties();
        hibernateProperties.put("hibernate.dialect", DIALECT);
        hibernateProperties.put("hibernate.hbm2ddl.auto", HBM2DLL);
        hibernateProperties.put("hibernate.show_sql", HSHOW_SQL);
        hibernateProperties.put("hibernate.proc.param_null_passing", PROC_NULL_PASSING);
        hibernateProperties.put("hibernate.enable_lazy_load_no_trans",true);
        sessionFactory.setHibernateProperties(hibernateProperties);
        return sessionFactory;
    }
	
	/** Define here is the Spring Data JPA entity Manager Factory configuration(s) 
	 * @throws PropertyVetoException 
	 * @throws SQLException **/
    @Bean
    public LocalContainerEntityManagerFactoryBean userEntityManager() throws NamingException, PropertyVetoException, SQLException{

    	HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
        jpaVendorAdapter.setGenerateDdl(true);
        jpaVendorAdapter.setShowSql(true);
        
        HashMap<String, Object> jpaProperties = new HashMap<>();
        jpaProperties.put("hibernate.dialect", DIALECT);
        jpaProperties.put("hibernate.hbm2ddl.auto", HBM2DLL);
        jpaProperties.put("hibernate.show_sql", HSHOW_SQL);
        jpaProperties.put("hibernate.proc.param_null_passing", PROC_NULL_PASSING);
        jpaProperties.put("hibernate.enable_lazy_load_no_trans",true);
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource());
        factoryBean.setJpaDialect(new HibernateJpaDialect());
        factoryBean.setJpaVendorAdapter(jpaVendorAdapter);
        factoryBean.setPersistenceUnitName("persistenceUnit");
        factoryBean.setJpaPropertyMap(jpaProperties);
        factoryBean.setPackagesToScan(new String[] {"com.shoppingcart.model"});
        factoryBean.afterPropertiesSet();
        return factoryBean;
    }
	
	@Bean
	public PlatformTransactionManager userTransactionManager() throws NamingException, PropertyVetoException, SQLException{
		 
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory( userEntityManager().getObject());
		return transactionManager; 
	}
	 
}