package com.shoppingcart.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoppingcart.dao.OrderDaoImpl;
import com.shoppingcart.dto.OrderDto;
import com.shoppingcart.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {
	
	protected static final Logger logger = LogManager.getLogger(CustomerServiceImpl.class);
	
	@Autowired
	OrderDaoImpl orderDaoImpl;
	
	@Override
	@Transactional
	public List<OrderDto> getCurstomerOrderList(int customerId) {
		List<OrderDto> result = null;
		try {
			result = orderDaoImpl.getCurstomerOrderList(customerId);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}
	
	@Override
	@Transactional
	public void saveCurstomerOrderData(OrderDto order) {
		logger.info("saveCurstomerOrderData | Start");
		try {
			logger.info("saveCurstomerOrderData | " + order.getOrderId() + " | " + order.getProductId() + " | " + order.getQuantity());
			orderDaoImpl.saveCurstomerOrderData(order);
			logger.info("saveCurstomerOrderData succeeded");
		} catch (Exception ex) {
			logger.error("saveCurstomerOrderData failed");
		} finally {
			logger.info("saveCurstomerOrderData | End");	
		}
	}
	
	@Override
	public void removeCurstomerOrderData(List<OrderDto> selectedRecords) {
		logger.info("removeCurstomerOrderData | Start");
		try {
			for (OrderDto order : selectedRecords) {
				logger.info("removeCurstomerOrderData | " + order.getOrderId() + " | " + order.getProductId() + " | " + order.getQuantity());
				orderDaoImpl.removeCurstomerOrderData(order.getOrderId());
			}
			logger.info("removeCurstomerOrderData succeeded");
		} catch (Exception ex) {
			logger.error("removeCurstomerOrderData failed");
		} finally {
			logger.info("removeCurstomerOrderData | End");	
		}
	}
}