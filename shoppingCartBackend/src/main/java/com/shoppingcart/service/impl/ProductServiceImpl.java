package com.shoppingcart.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoppingcart.dao.ProductDaoImpl;
import com.shoppingcart.dto.ProductDto;
import com.shoppingcart.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService {
	
	protected static final Logger logger = LogManager.getLogger(ProductServiceImpl.class);
	
	@Autowired
	ProductDaoImpl productDaoImpl;
	
	@Override
	@Transactional
	public List<ProductDto> getProductList() {
		List<ProductDto> result = null;
		try {
			result = productDaoImpl.getProductList();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}
	
}