package com.shoppingcart.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shoppingcart.dao.CustomerDaoImpl;
import com.shoppingcart.dto.CustomerDto;
import com.shoppingcart.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {
	
	protected static final Logger logger = LogManager.getLogger(CustomerServiceImpl.class);
	
	@Autowired
	CustomerDaoImpl customerDaoImpl;
	
	@Override
	@Transactional
	public List<CustomerDto> getCustomerList() {
		List<CustomerDto> result = null;
		try {
			result = customerDaoImpl.getCustomerList();
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return result;
	}
	
	@Override
	@Transactional
	public void saveCustomerData(CustomerDto customer) {
		logger.info("saveCustomerData | Start");
		try {
			logger.info("saveCustomerData | " + customer.getFullName() + " | " + customer.getCustomerId());
			customerDaoImpl.saveCustomerData(customer);
			logger.info("saveCustomerData succeeded");
		} catch (Exception ex) {
			logger.error("saveCustomerData failed");
		} finally {
			logger.info("saveCustomerData | End");	
		}
	}
}