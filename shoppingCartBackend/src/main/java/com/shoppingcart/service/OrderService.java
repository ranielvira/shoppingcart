package com.shoppingcart.service;

import java.util.List;

import com.shoppingcart.dto.OrderDto;

public interface OrderService {
	
	public List<OrderDto> getCurstomerOrderList(int customerId);
	void saveCurstomerOrderData(OrderDto customer);
	void removeCurstomerOrderData(List<OrderDto> selectedRecords);

}