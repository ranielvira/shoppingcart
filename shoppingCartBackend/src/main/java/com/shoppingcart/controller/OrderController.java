package com.shoppingcart.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shoppingcart.dto.CommitStatus;
import com.shoppingcart.dto.OrderDto;
import com.shoppingcart.service.OrderService;

@RestController
public class OrderController {
	
	protected static final Logger logger = LogManager.getLogger(OrderController.class);
	
	@Autowired
	OrderService orderService;
	
	@RequestMapping(value = "${apiEndpoint}/order/getCurstomerOrderList", method = RequestMethod.GET)
	public List<OrderDto> getCurstomerOrderList(@RequestParam String customerId){
		logger.info("BEGIN - Get Curstomer Order List");
		
		List<OrderDto> result = null;
		try {
			result = orderService.getCurstomerOrderList(Integer.valueOf(customerId));
			logger.info("Get Curstomer Order List success - record size : " + result.size());
		} catch (Exception e) {
			logger.error("Get Curstomer Order List failed", e);
		}
		logger.info("END - Get Curstomer Order List");
		return result;
    }
	
	@RequestMapping(value = "${apiEndpoint}/order/saveCurstomerOrderData", method = RequestMethod.POST)
	public CommitStatus saveCurstomerOrderData(@RequestBody OrderDto order){
		logger.info("BEGIN - saveCurstomerOrderData");

		CommitStatus status = new CommitStatus();
		try {
			orderService.saveCurstomerOrderData(order);
			status = new CommitStatus("success", "");
			logger.info("saveCurstomerOrderData successful");
		} catch (Exception e) {
			status = new CommitStatus("failed","");
			logger.error("saveCurstomerOrderData failed", e);
		}
		logger.info("END - saveCurstomerOrderData"); 
		return status;
	}
	
	@RequestMapping(value = "${apiEndpoint}/order/removeCurstomerOrderData", method = RequestMethod.POST)
	public CommitStatus removeCurstomerOrderData(@RequestBody List<OrderDto> selectedRecords){
		logger.info("BEGIN - removeCurstomerOrderData");

		CommitStatus status = new CommitStatus();
		try {
			orderService.removeCurstomerOrderData(selectedRecords);
			status = new CommitStatus("success", "");
			logger.info("removeCurstomerOrderData successful");
		} catch (Exception e) {
			status = new CommitStatus("failed","");
			logger.error("removeCurstomerOrderData failed", e);
		}
		logger.info("END - removeCurstomerOrderData"); 
		return status;
	}
}