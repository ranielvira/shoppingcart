package com.shoppingcart.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shoppingcart.dto.CommitStatus;
import com.shoppingcart.dto.CustomerDto;
import com.shoppingcart.service.CustomerService;

@RestController
public class CustomerController {
	
	protected static final Logger logger = LogManager.getLogger(CustomerController.class);
	
	@Autowired
	CustomerService customerService;
	
	@RequestMapping(value = "${apiEndpoint}/customer/getCustomerList", method = RequestMethod.GET)
	public List<CustomerDto> getCustomerList(){
		logger.info("BEGIN - Get CustomerList");
		
		List<CustomerDto> result = null;
		try {
			result = customerService.getCustomerList();
			logger.info("Get CustomerList success - record size : " + result.size());
		} catch (Exception e) {
			logger.error("Get CustomerList failed", e);
		}
		logger.info("END - Get CustomerList");
		return result;
    }
	
	@RequestMapping(value = "${apiEndpoint}/customer/saveCustomerData", method = RequestMethod.POST)
	public CommitStatus saveCustomerData(@RequestBody CustomerDto customer){
		logger.info("BEGIN - saveCustomerData");

		CommitStatus status = new CommitStatus();
		try {
			customerService.saveCustomerData(customer);
			status = new CommitStatus("success", "");
			logger.info("saveCustomerData successful");
		} catch (Exception e) {
			status = new CommitStatus("failed","");
			logger.error("saveCustomerData failed", e);
		}
		logger.info("END - saveCustomerData"); 
		return status;
	}
}