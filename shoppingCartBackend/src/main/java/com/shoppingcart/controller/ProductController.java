package com.shoppingcart.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shoppingcart.dto.ProductDto;
import com.shoppingcart.service.ProductService;

@RestController
public class ProductController {
	
	protected static final Logger logger = LogManager.getLogger(ProductController.class);
	
	@Autowired
	ProductService productService;
	
	@RequestMapping(value = "${apiEndpoint}/product/getProductList", method = RequestMethod.GET)
	public List<ProductDto> getProductList(){
		logger.info("BEGIN - Get ProductList");
		
		List<ProductDto> result = null;
		try {
			result = productService.getProductList();
			logger.info("Get ProductList success - record size : " + result.size());
		} catch (Exception e) {
			logger.error("Get ProductList failed", e);
		}
		logger.info("END - Get ProductList");
		return result;
    }
	
}